﻿using System;
using System.Collections.Generic;

namespace Calculator
{
    class Calculator
    {
        public static double Calculate(string sum)
        {
            List<string> numberList = new List<string>(sum.Split(' '));

            int index = 0;
            double num1, num2, result;
            string operation;

            if (numberList.Contains("("))
            {
                while (numberList.Contains("("))
                {
                    numberList = CalculateWithBracket(numberList);
                }
            }

            while (numberList.Count > 1)
            {
                if (numberList.Contains("*") || numberList.Contains("/"))
                {
                    index = numberList.FindIndex(x => x.Equals("*") || x.Equals("/"));
                }
                else
                {
                    index = numberList.FindIndex(x => x.Equals("+") || x.Equals("-"));
                }

                result = 0;
                num1 = Convert.ToDouble(numberList[index - 1]);
                num2 = Convert.ToDouble(numberList[index + 1]);
                operation = numberList[index];

                switch (operation)
                {
                    case "+":
                        result = num1 + num2;
                        break;

                    case "-":
                        result = num1 - num2;
                        break;

                    case "*":
                        result = num1 * num2;
                        break;

                    case "/":
                        result = num1 / num2;
                        break;

                    default:
                        throw new Exception("Invalid operator");
                }

                numberList.RemoveAt(index + 1);
                numberList.RemoveAt(index);
                numberList[index - 1] = result.ToString();
            }

            return Math.Round(Convert.ToDouble(numberList[0]), 1);
        }

        private static List<string> CalculateWithBracket(List<string> numberList)
        {
            int firstIndex = numberList.FindLastIndex(x => x.Equals("("));
            int lastIndex = numberList.FindIndex(x => x.Equals(")"));

            string[] arr = new string[lastIndex - firstIndex - 1];
            numberList.CopyTo(firstIndex + 1, arr, 0, lastIndex - firstIndex - 1);

            numberList[firstIndex] = Calculate(string.Join(" ", arr)).ToString();

            for (int i = lastIndex; i > firstIndex; i--)
            {
                numberList.RemoveAt(i);
            }

            return numberList;
        }
    }
}
