﻿using System;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please input string for calculation: ");
            string userInput = Console.ReadLine();

            while (userInput != "exit") 
            {
                if (!string.IsNullOrWhiteSpace(userInput))
                {
                    try
                    {
                        Console.WriteLine("Result: " + Calculator.Calculate(userInput));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Environment.Exit(-1);
                    }
                }
                else
                {
                    Console.WriteLine("Input cannot be empty.");
                }

                Console.WriteLine(Environment.NewLine);
                Console.WriteLine("Please input string for calculation: ");
                userInput = Console.ReadLine();
            }
        }
    }
}
